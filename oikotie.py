import time
import re
import json
from datetime import datetime
import timeit

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import gspread
from oauth2client.service_account import ServiceAccountCredentials
import smtplib



def config(key):

	with open("config.json", "r") as file:
		config_data = json.load(file)

	return config_data[key]



def scrape(job_name, sheet_no, url):

	results = [] # list for storing dictionary for each downloaded ad

	print("\nStarting job {}.".format(job_name))

	options = Options()
	options.headless = True # True hides browser

	# path to Chrome driver from separate json file
	driver_path = config("driver_path")

	# open new browser
	driver = webdriver.Chrome(options = options, executable_path = driver_path)
	driver.get(url)

	time.sleep(1) # let page load

	# get total number of ads and pages
	ad_count = driver.find_element_by_xpath("//span[contains(@class, 'search-result-controls__found')]").get_attribute("innerHTML")
	print("   Total ads: {}.".format(ad_count))

	page_count_raw = driver.find_element_by_tag_name("pagination-indication").find_elements_by_tag_name("span")[2].get_attribute("innerHTML")
	page_count = re.findall(r"[0-9]+$", page_count_raw)[0] # e.g. pick out 3 from "1/3"
	print("   Total pages: {}.".format(page_count))


	try: # wrapping the whole scraper

		for n in range(1, int(page_count) + 1):

			# modify url to include pagination
			url_split = url.split("pagination=1")
			new_url = "{}pagination={}{}".format(url_split[0], n, url_split[1])

			# load search page
			print("   Opening search page {}.".format(n))
			driver.get(new_url)
			
			# wait while search page loads
			# depending on loading speed, risk to lose ads - increase if needed
			time.sleep(3.5)

			# (re)create temporary lists for storing scraped ad data
			street = []
			apartment_type = []
			area = []
			price = []
			size = []
			link = []
			id_number = []

			# pick out relevant elements
			street_raw = driver.find_elements_by_class_name("ot-card__street")
			area_apartment_type_raw = driver.find_elements_by_xpath("//div[contains(@class, 'ot-card__text')]")
			price_raw = driver.find_elements_by_class_name("ot-card__price")
			size_raw = driver.find_elements_by_class_name("ot-card__size")
			links_raw = driver.find_elements_by_xpath("//a[contains(@class, 'ot-card')]")


			#PROCESS RELEVANT ELEMENTS

			for x in street_raw:
				a = x.get_attribute("innerHTML")
				street.append(a)

			for i, x in enumerate(area_apartment_type_raw):
				i += 1
				if (i % 2) == 0: #check if even number, we want to pick out every second element: 1, 3, 5 etc
					a = x.get_attribute("innerHTML")
					apartment_type.append(a)
				else:
					b = x.find_elements_by_xpath(".//span")
					c = b[0].get_attribute("innerHTML")
					area.append(c)

			for x in price_raw:
				a = x.get_attribute("innerHTML").replace("&nbsp","").replace(";","") # clean a bit
				b = re.sub("\D", "", a) # leave only digits
				c = int(b) # convert to int
				price.append(c)

			for x in size_raw:
				a = x.get_attribute("innerHTML")
				b = a[:-3] # strip "m2" at the end
				c = b.replace(",",".")
				d = float(c) # convert to float
				size.append(d)

			for x in links_raw:
				a = x.get_attribute("href") # get link
				link.append(a)

				b = re.match(".*?([0-9]+)$", a).group(1) # get is number at the end of the url
				c = int(b) # convert to int
				id_number.append(c)


			# combine above data into a dict, then add dict to results list

			for x in range(0, len(street)):

				date = datetime.today().strftime('%Y-%m-%d %H:%M')

				temp_dict = {"id_number" : id_number[x], "street" : street[x], "area" : area[x], "apartment_type" : apartment_type[x], "price" : price[x], "size" : size[x], "price_per_sqm" : price[x] / size[x], "link" : link[x], "date" : date, "floor": None, "balcony": None, "condition": None, "payment": None, "payment_per_sqm": None, "new_entry": None, "furniture": None}

				results.append(temp_dict)

			time.sleep(3)


		print("   Succesfully downloaded {} {}.".format(len(results), "ad" if len(results) == 1 else "ads"))

		if int(ad_count) != len(results):
			print("   ERROR: Not all available ads downloaded.")



		# OPEN SPREADSHEET, GET EXISTING IDS

		scope = ["https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/drive"]
		creds = ServiceAccountCredentials.from_json_keyfile_name("config-oikotie-40321eba2a0a.json", scope)
		client = gspread.authorize(creds)

		# sheet name from separate json file
		sheet = client.open(config("sheet_name"))

		# get sheet
		sheet_instance = sheet.get_worksheet(sheet_no)

		# get existing ids in spreadsheet
		existing_ids = [item for item in sheet_instance.col_values(1) if item]
		existing_ids = existing_ids[1:] # delete first row which contains column headers
		existing_ids = [int(item) for item in existing_ids] # convert string to int

		# check if ad already exists in spreadsheet
		for item in results:
			if item["id_number"] not in existing_ids:
				item["new_entry"] = True
			else:
				item["new_entry"] = False


		# SCRAPE INDIVIDUAL ADS

		for item in results:
			if item["new_entry"]:

				url = item["link"]

				# open ad page
				driver.get(url)

				print("   Opening ad ({}, {} euro).".format(item["street"], item["price"]))

				# pick out relevant elements from table
				first_raw = driver.find_elements_by_xpath("//dt[contains(@class, 'info-table__title')]")
				second_raw = driver.find_elements_by_xpath("//dd[contains(@class, 'info-table__value')]")

				temp_ad_dict = {}

				# populate temp_ad_dict where dt is key and dd is value
				for i, x in enumerate(first_raw):
					a = x.get_attribute("innerHTML")
					b = second_raw[i].get_attribute("innerHTML")
					temp_ad_dict[a] = b


				try:
					item["floor"] = temp_ad_dict["Kerros"]
				except:
					item["floor"] =  "N/A"


				try:
					item["balcony"] = temp_ad_dict["Parveke"]
				except:
					item["balcony"] = "N/A"


				try:
					item["condition"] = temp_ad_dict["Kunto"]
				except:
					item["condition"] = "N/A"


				try:
					a = temp_ad_dict['Hoitovastike'].replace(",", ".")
					b = re.findall('\d+|\.', a)
					c = ''.join(b)
					d = float(c)
					item["payment"] = d
				except:
					item["payment"] = "N/A"


				try: 
					item["payment_per_sqm"] = item["payment"] / item["size"] #calculate payment per sqm
				except:
					item["payment_per_sqm"] = "N/A"
				

				try:
					item["furniture"] = temp_ad_dict['Vuokrataan kalustettuna']
				except:
					item["furniture"] = "N/A"


		# ADD ADS TO SPREADSHEET

		new_counter = 0 # for status

		for item in results:

			# check if new ad, then add row to spreadsheet
			if item["new_entry"]:
				new_row = (item["id_number"], item["street"], item["area"], item["apartment_type"], item["price"], item["size"], item["price_per_sqm"], item["floor"], item["balcony"], item["condition"], item["payment"], item["payment_per_sqm"], item["furniture"], item["link"], item["date"])

				sheet_instance.append_row(new_row)
				new_counter += 1
		

		# print status and add to email message

		status = ""

		if new_counter == 0:
			status = "No new ads found."
		else:
			status = "{} new {} added to sheet {}.".format(new_counter, "ad" if new_counter == 1 else "ads", sheet_no) 

		print("   {}\n".format(status))

		email_message.append([job_name, status]) # add status to email message list


		# write to scraping log on sheet 0
		# columen headers: timestamp, job_name, sheet_no, url, status

		sheet_instance_log = sheet.get_worksheet(0)
		timestamp = datetime.today().strftime('%Y-%m-%d %H:%M')
		new_row_log = (timestamp, job_name, sheet_no, url, status)
		sheet_instance_log.append_row(new_row_log)


		# close driver
		driver.quit()

		# wait a little before next scraper, if any
		time.sleep(2)

	except:
		raise



def send_email(from_addr, to_addr_list, cc_addr_list, subject, message, login, password, smtpserver):
	
	header = "From: {}\n".format(from_addr)
	header += "To: {}\n".format(",".join(to_addr_list))
	header += "Cc: {}\n".format(",".join(cc_addr_list))
	header += "Subject: {}\n\n".format(subject)
	message = header + message

	server = smtplib.SMTP(smtpserver)
	server.starttls()
	server.login(login,password)
	errors = server.sendmail(from_addr, to_addr_list, message)
	server.quit()

	return errors



def main():

	# set global variable for email

	global email_message
	email_message = []


	# scraping log is on sheet_no 0
	# column headers on other sheets: id_number, street, area, apartment_type, price, size, price_per_sqm, floor, balcony, condition, payment, payment_per_sqm, furniture, link, first_date

	scrape("kallio_sale_25_sqm", 1, "https://asunnot.oikotie.fi/myytavat-asunnot?pagination=1&locations=%5B%5B335403,4,%22Alppila,%20Helsinki%22%5D,%5B1650,4,%22Alppiharju,%20Helsinki%22%5D,%5B1660,4,%22Vallila,%20Helsinki%22%5D,%5B1649,4,%22Kallio,%20Helsinki%22%5D%5D&size%5Bmax%5D=25&cardType=100")

	scrape("kallio_rent_25_sqm", 2, "https://asunnot.oikotie.fi/vuokrattavat-asunnot?pagination=1&locations=%5B%5B1649,4,%22Kallio,%20Helsinki%22%5D%5D&cardType=101&size%5Bmax%5D=25&cardType=100")

	scrape("karis_sale_25_sqm", 3, "https://asunnot.oikotie.fi/myytavat-asunnot?pagination=1&locations=%5B%5B5969964,4,%22Karjaa,%20Raasepori%22%5D%5D&cardType=100&size%5Bmax%5D=25&cardType=100")

	scrape("karis_rent_25_sqm", 4, "https://asunnot.oikotie.fi/vuokrattavat-asunnot?pagination=1&locations=%5B%5B5969964,4,%22Karjaa,%20Raasepori%22%5D%5D&cardType=101&size%5Bmax%5D=25&cardType=100")

	scrape("lohja_sale_25_sqm", 5, "https://asunnot.oikotie.fi/myytavat-asunnot?pagination=1&locations=%5B%5B229,6,%22Lohja%22%5D%5D&cardType=100&size%5Bmax%5D=25&cardType=100")

	scrape("lohja_rent_25_sqm", 6, "https://asunnot.oikotie.fi/vuokrattavat-asunnot?pagination=1&locations=%5B%5B229,6,%22Lohja%22%5D%5D&cardType=101&size%5Bmax%5D=25&cardType=100")

	scrape("karis_sale_26_to_40_sqm", 7, "https://asunnot.oikotie.fi/myytavat-asunnot?pagination=1&locations=%5B%5B5969964,4,%22Karjaa,%20Raasepori%22%5D%5D&cardType=100&size%5Bmin%5D=26&size%5Bmax%5D=40&cardType=100")

	scrape("karis_rent_26_to_40_sqm", 8, "https://asunnot.oikotie.fi/vuokrattavat-asunnot?pagination=1&locations=%5B%5B5969964,4,%22Karjaa,%20Raasepori%22%5D%5D&cardType=101&size%5Bmin%5D=26&size%5Bmax%5D=40&cardType=100")

	scrape("lohja_sale_26_to_40_sqm", 9, "https://asunnot.oikotie.fi/myytavat-asunnot?pagination=1&locations=%5B%5B229,6,%22Lohja%22%5D%5D&cardType=100&size%5Bmin%5D=26&size%5Bmax%5D=40&cardType=100")

	scrape("lohja_rent_26_to_40_sqm", 10, "https://asunnot.oikotie.fi/vuokrattavat-asunnot?pagination=1&locations=%5B%5B229,6,%22Lohja%22%5D%5D&cardType=101&size%5Bmin%5D=26&size%5Bmax%5D=40&cardType=100")


	# after scraping, send plain email with results to uggla@jippii.fi
	# sender address and app password in separate json file 
	# (https://support.google.com/mail/answer/185833?hl=en)

	email_message_string = ""

	for x in email_message:
		email_message_string += "{:<27} {}\n".format(x[0], x[1])

	# using gmail's smtp server to send email

	send_email(from_addr = config("gmail_from"), to_addr_list = ["uggla@jippii.fi"], cc_addr_list = [""], subject = "Oikotie scraper", message = email_message_string, login = config("gmail_username"), password = config("gmail_password"), smtpserver="smtp.gmail.com:587")



if __name__ == "__main__":

	#time the full process
	elapsed_time = timeit.timeit(main, number=1)
	print("Elapsed time: {} seconds.\n".format(round(elapsed_time, 0)))	
# Housing ads scraper for www.oikotie.fi

A friend of mine wanted to keep track of housing ads in southern Finland. He wanted to automatically download data about location, price, condition etc. - as well as some basic calculations - to a Google spreadsheet. I coded this Selenium-based python scraper specifically for the Finnish Craigslist-like site *Oikotie*. The site uses Javascript to present its results, thus Selenium was needed.

The scraper takes a search URL like this as input:

> https://asunnot.oikotie.fi/vuokrattavat-asunnot?pagination=1&locations=%5B%5B1649,4,%22Kallio,%20Helsinki%22%5D%5D&cardType=101&size%5Bmax%5D=25&cardType=100

The terminal output could look like this:

```
Starting job kallio_rent_25_sqm.
   Total ads: 65.
   Total pages: 3.
   Opening search page 1.
   Opening search page 2.
   Opening search page 3.
   Succesfully downloaded 65 ads.
   Opening ad (Helsinginkatu 9, 780 euro).
   Opening ad (Wallininkatu 5, 890 euro).
   Opening ad (Sturenkatu 13 A, 685 euro).
   Opening ad (Alppikatu 5, 780 euro).
   4 new ads added to sheet 2.
```

Finally, the scraped data is added to a Google spreadsheet. Here is an [example](https://docs.google.com/spreadsheets/d/1Lk5kIHZfJ7s-gkNQcaCrj2PKsyK9rKK0T5JE_XzXoNk/edit?usp=sharing).

I got access to the spreadsheet using this clear [step-by-step guide](https://shaikhu.com/how-to-access-google-sheets-using-python-and-gspread).

I'm running the scraper on Mac OS 11 with the latest version of [Chromedriver](https://chromedriver.chromium.org/downloads).

<!--
1. Get total number of ads and pages for search
1. Get basic data from summary page
1. One by one, open each ad and get detailed data
1.  
etc
-->
